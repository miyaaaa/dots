require("gitsigns").setup({
	signcolumn = false,
	current_line_blame = false,
	numhl = false,
})

-- <leader> <g> <m> -> Open git commit message
